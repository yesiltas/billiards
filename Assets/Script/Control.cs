﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public enum CueState { Wait, MakeActive, UpdateRotation, PrepareShoot, Shoot }
    public CueState cueState = CueState.Wait;

    public GameObject cue;
    public WhiteBallManager ball;

    public Action<bool> ChangeBallState;

    public float speed;
    public float maxForce;
    public float time;

    BallColor targetColor = BallColor.White;
    List<BallController> TargetBalls = new List<BallController>();

    float force = 0;
    bool isReady = false;

    public void Again()
    {
        TargetBalls.Clear();
        cueState = CueState.Wait;
        targetColor = BallColor.White;
        isReady = false;
    }

    public void StartControl()
    {
        cueState = CueState.MakeActive;
    }

    public void StopControl()
    {
        cueState = CueState.Wait;
    }

    public void PrepareShoot(float sliderValue)
    {
        force = maxForce * sliderValue;
        cueState = CueState.PrepareShoot;
    }

    void Update()
    {
        switch (cueState)
        {
            case CueState.Wait:
                CueWait();
                break;

            case CueState.MakeActive:
                cue.SetActive(true);
                cueState = CueState.UpdateRotation;
                break;

            case CueState.UpdateRotation:
                if (Input.GetMouseButton(0))
                {
                    RaycastHit hit;
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                    {
                        if (hit.transform.tag == "Ground")
                        {
                            CueUpdateRotation();
                        }
                    }
                }
                break;

            case CueState.PrepareShoot:
                cueState = CueState.Shoot;
                StartCoroutine(ExecuteAfterTime());
                break;

            case CueState.Shoot:
                CueShoot();
                break;

        }
    }

    private void CueShoot()
    {
        if (isReady)
        {
            cue.transform.position += cue.transform.up * speed * Time.deltaTime;
        }
        else
        {
            cue.transform.position -= cue.transform.up * speed * Time.deltaTime;
        }
    }

    IEnumerator ExecuteAfterTime()
    {
        yield return new WaitForSeconds(time);
        isReady = true;
        yield return new WaitForSeconds(time + 0.01f);
        isReady = false;
        cueState = CueState.Wait;
        ball.Shoot(transform.forward, force, targetColor);
        yield return new WaitForSeconds(1f);
        ChangeBallState(true);
    }

    private void CueUpdateRotation()
    {
        Vector3 mousepos = Input.mousePosition;
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(mousepos.x, mousepos.y, 38.5f));
        position += new Vector3(0, 0.5f, 0);
        Vector3 direction = transform.position - position;
        transform.forward = direction.normalized;
        //transform.rotation = Quaternion.Euler(0, transform.rotation.y * Mathf.Rad2Deg, 0);
    }

    private void CueWait()
    {
        transform.position = ball.transform.position;
        transform.forward = ball.transform.forward;
        cue.transform.position = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z - 28);
        cue.SetActive(false);
    }

    public BallColor GetTargetColor()
    {
        return targetColor;
    }

    public void SetTargetColor(BallColor targetColor, List<BallController> TargetBalls)
    {
        this.targetColor = targetColor;
        this.TargetBalls = TargetBalls;
    }
    public void SetTargetColor(BallColor targetColor)
    {
        this.targetColor = targetColor;
    }

    public bool CheckList()
    {
        foreach (var item in TargetBalls)
        {
            if (item.ballState != BallState.NotActive)
            {
                return false;
            }
        }

        return true;
    }
}
