﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BallColor
{
    White,
    Black,
    Red,
    Yellow
}

public enum BallState
{
    Staying,
    Moving,
    NotActive
}

public enum Turn
{
    Player,
    Computer
}