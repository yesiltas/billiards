﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    public Action ChangeTurn;
    public Action Foul;
    public Action BlackInBasket;
    public Action<List<BallColor>> CheckBalls;
    public Action ChangeAngle;
    public Action isNotFoul;
    public Action<List<BallColor>> CheckBallsSimulate;

    public List<BallController> ballControllers;

    public ComputerControl computerControl;

    bool ballsMoving = false;
    bool notStay = true;

    public WhiteBallManager whiteBall;

    public void StartGame()
    {
        ballsMoving = false;
        notStay = true;
        currentBallsInBasket.Clear();
        foreach (var item in ballControllers)
        {
            item.StartGame();
        }
    }

    public void Again()
    {
        foreach (var item in ballControllers)
        {
            item.Again();
        }
    }

    public void ChangeBallsState(bool ballsMoving)
    {
        this.ballsMoving = ballsMoving;
    }

    List<BallColor> currentBallsInBasket = new List<BallColor>();

    public void BallInBasket(BallController ball)
    {
        currentBallsInBasket.Add(ball.ballColor);
        ball.gameObject.SetActive(false);
    }

    public void BallInBasketSimulate(BallController ball)
    {
        currentBallsInBasket.Add(ball.ballColor);
    }

    public void AnyBallInBasket()
    {
        bool isFoul = false;

        if (currentBallsInBasket.Count > 0)
        {
            foreach (var item in currentBallsInBasket)
            {
                if (item == BallColor.Black)
                {
                    BlackInBasket();
                    return;
                }
                else if (item == BallColor.White)
                {
                    Foul();
                    isFoul = true;
                    break;
                }
            }

            if (isFoul == false)
            {
                isFoul = whiteBall.CheckIfFoul(false);
            }

            if (isFoul == false)
            {
                CheckBalls(currentBallsInBasket);
            }
        }
        else
        {
            if (whiteBall.CheckIfFoul(false) == false)
            {
                ChangeTurn();
            }

        }
        currentBallsInBasket.Clear();
    }

    public void AnyBallInBasketSimulate()
    {
        bool isFoul = false;
        bool doNotControl = false;
        SimulateEnd();

        if (currentBallsInBasket.Count > 0)
        {
            foreach (var item in currentBallsInBasket)
            {
                if (item == BallColor.Black)
                {
                    if (computerControl.CheckList() == false)
                    {                     
                        ChangeAngle();
                        doNotControl = true;                       
                    }

                    break;
                }
                else if (item == BallColor.White)
                {
                   
                    ChangeAngle();
                    isFoul = true;
                    break;
                }
            }

            if (isFoul == false && doNotControl == false)
            {
                if (whiteBall.CheckIfFoul(true))
                {
                  
                    ChangeAngle();
                }             
            }

            if (isFoul == false && doNotControl == false)
            {
                
                CheckBallsSimulate(currentBallsInBasket);
            }
        }
        else
        {
            if (whiteBall.CheckIfFoul(true) == false)
            {
               
                isNotFoul();
            }
            else
            {
                
                ChangeAngle();
            }
        }
        currentBallsInBasket.Clear();
    }

    private void Update()
    {
        if (ballsMoving)
        {
            foreach (var item in ballControllers)
            {
                if (item.ballState == BallState.Moving)
                {
                    notStay = true;
                    break;
                }
                else
                {
                    notStay = false;
                }
            }
            if (notStay == false)
            {
                AnyBallInBasket();
                ballsMoving = false;
            }
        }
    }

    public List<BallController> GetTargetsList(BallColor ballColor)
    {
        List<BallController> targets = new List<BallController>();

        foreach (var item in ballControllers)
        {
            if (item.ballColor == ballColor)
            {
                targets.Add(item);
            }
        }

        return targets;
    }

    public void SimulateStart()
    {
        foreach (var item in ballControllers)
        {
            if (item.ballState != BallState.NotActive)
            {
                item.SimulateStart();
            }
        }
        float timer = 5;
        while (true)
        {

            Physics.Simulate(Time.fixedDeltaTime);
            foreach (var item in ballControllers)
            {
                if (item.GetComponent<Rigidbody>().velocity != Vector3.zero)
                {
                    notStay = true;
                    break;
                }
                else
                {
                    notStay = false;
                }
            }
            if (notStay == false)
            {
                break;             
            }
            timer -= Time.fixedDeltaTime;
        }
        AnyBallInBasketSimulate();
    }

    public void SimulateEnd()
    {
        foreach (var item in ballControllers)
        {
            if (item.ballState != BallState.NotActive)
            {
                item.SimulateEnd();
            }
        }
    }
}
