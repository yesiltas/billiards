﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBallManager : MonoBehaviour
{
    public enum Foul
    {
        WhiteBallInBasket,
        NoCollisionWithTargets,
        None
    }
    public Foul foul = Foul.None;

    public Action NoCollisionFoul;
    public Action StartControl;
    public Action StopContol;

    BallColor ballColor = BallColor.Red;
    Rigidbody rb;
    Vector3 startPosition = Vector3.zero;

    bool isFoul = true;
    bool isPlayerTurn = false;
    bool isPlayerChangingPosition = false;

    private void Awake()
    {
        startPosition = transform.position;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void StartGame()
    {
        foul = Foul.None;
        startPosition = transform.position;

        isFoul = true;
        isPlayerTurn = false;
        isPlayerChangingPosition = false;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Ball")
        {
            if (ballColor == BallColor.White)
            {
                isFoul = false;
            }
            if (collision.gameObject.GetComponent<BallController>().ballColor == ballColor)
            {
                isFoul = false;
            }
        }
    }

    public bool CheckIfFoul(bool isSimulate)
    {
        if (isSimulate)
        {
            return isFoul;
        }
        else
        {
            if (isFoul)
            {
                NoCollisionFoul();
                if (isPlayerTurn)
                    foul = Foul.NoCollisionWithTargets;
                StartControl();
                return true;
            }
        }
        return false;
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (foul == Foul.NoCollisionWithTargets)
            {
                isPlayerChangingPosition = !isPlayerChangingPosition;
            }

            if (isPlayerChangingPosition)
            {
                StopContol();
            }

            else
            {
                StartControl();
            }
        }
    }

    public void CheckTurn(Turn turn)
    {
        switch (turn)
        {
            case Turn.Player:
                isPlayerTurn = true;
                break;
            case Turn.Computer:
                isPlayerTurn = false;
                break;
        }
    }

    public void Shoot(Vector3 direction, float force, BallColor ballColor)
    {
        foul = Foul.None;
        this.ballColor = ballColor;
        rb.AddForce(direction * force);
        isFoul = true;
    }

    public void Replacement(Turn turn)
    {
        switch (turn)
        {
            case Turn.Player:
                rb.velocity = Vector3.zero;
                StopContol();
                foul = Foul.WhiteBallInBasket;
                transform.position = startPosition;
                gameObject.SetActive(true);
                break;

            case Turn.Computer:
                transform.position = startPosition;
                rb.velocity = Vector3.zero;
                GetComponent<BallController>().ballState = BallState.Staying;
                break;
        }
    }

    private void Update()
    {
        switch (foul)
        {
            case Foul.WhiteBallInBasket:
                ChangePosition();
                if (Input.GetMouseButtonDown(0))
                {
                    foul = Foul.None;
                    StartControl();
                }
                break;
            case Foul.None:
                break;
        }
        if (isPlayerChangingPosition)
        {
            ChangePosition();
        }
    }

    private void ChangePosition()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.transform.tag == "Ground")
            {
                Vector3 mousepos = Input.mousePosition;
                Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(mousepos.x, mousepos.y, 25f));
                position += new Vector3(0, 0.5f, 0);
                transform.position = position;
            }
        }
    }
}
