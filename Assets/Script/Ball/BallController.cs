﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public BallColor ballColor;
    public BallState ballState;

    public BallManager ballManager;

    Rigidbody rb;

    Vector3 startPosition = Vector3.zero;
    Vector3 simulatePosition = Vector3.zero;

    bool isSimulate = false;

    private void Awake()
    {
        startPosition = transform.position;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void StartGame()
    {
        transform.position = startPosition;
    }

    public void Again()
    {
        rb.velocity = Vector3.zero;
        transform.position = startPosition;
        ballState = BallState.Staying;
        gameObject.SetActive(true);
    }

    public void SimulateStart()
    {
        simulatePosition = transform.position;
        isSimulate = true;
    }

    public void SimulateEnd()
    {
        rb.velocity = Vector3.zero;
        transform.position = simulatePosition;
        isSimulate = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isSimulate)
        {
            ballManager.BallInBasketSimulate(GetComponent<BallController>());
        }
        else
        {
            ballState = BallState.NotActive;
            rb.velocity = Vector3.zero;
            ballManager.BallInBasket(GetComponent<BallController>());
        }
    }

    private void Update()
    {
        if (ballState != BallState.NotActive)
        {
            if (rb.velocity == Vector3.zero)
            {
                ballState = BallState.Staying;
            }

            else
            {
                ballState = BallState.Moving;
            }
        }
       

    }
}
