﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject gamePanel;
    public GameObject finishPanel;
    public GameObject slider;

    public Text startText;
    public Text finishText;
    public Text winLoseText;

    public GameManager gameManager;

    private void Awake()
    {
        startText.text = "Tap To Play";
        finishText.text = "Play Again";
    }

    private void Start()
    {
        gameManager.StartSliider += StartSlider;
        gameManager.GameFinish += OnGameFinished;
    }

    public void StartGame()
    {
        OpenGamePanel();
        gameManager.StartGame();      
    }
    
    public void Again()
    {
        gameManager.Again();
        OpenStartPanel();   
    }

    private void OnGameFinished(Turn turn)
    {
        switch (turn)
        {
            case Turn.Player:
                winLoseText.text = "Player Win";
                break;
            case Turn.Computer:
                winLoseText.text = "Computer Win";
                break;
        }
        OpenFinishPanel();
    }

    public void SliderControl()
    {
        float sliderValue = slider.GetComponent<Slider>().value;
        gameManager.PrepareShot(sliderValue);
        gameManager.StartRayCast();
        StopSlider();
    }

    public void StopRayCast()
    {
        gameManager.StopRayCast();
    }

    void StartSlider()
    {
        slider.SetActive(true);
    }

    void StopSlider()
    {
        slider.GetComponent<Slider>().value = 0;
        slider.SetActive(false);
    }

    private void OpenGamePanel()
    {
        startPanel.SetActive(false);
        gamePanel.SetActive(true);
        finishPanel.SetActive(false);
    }
    
    private void OpenFinishPanel()
    {
        startPanel.SetActive(false);
        gamePanel.SetActive(false);
        finishPanel.SetActive(true);
    }
    
    private void OpenStartPanel()
    {
        startPanel.SetActive(true);
        gamePanel.SetActive(false);
        finishPanel.SetActive(false);
    }

}
