﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerControl : MonoBehaviour
{
    public GameObject computerCue;
    public WhiteBallManager ball;
    public Action<bool> ChangeBallStateByComputer;
    public Action SimulateStartAction;
    public Action SimulateEndAction;

    public float speed;
    public float time;

    BallColor targetColor = BallColor.White;

    List<BallController> TargetBalls = new List<BallController>();

    bool isReady = false;
    bool isShooting = false;

    float shootAngle = 0;
    float holdAngle = -1;
    float force = 6000;
    float forceFactor = 0.25f;
    float holdForceFactor = 0;

    public void Again()
    {
        TargetBalls.Clear();
        targetColor = BallColor.White;
        isReady = false;
        isShooting = false;
    }

    public void StartControl()
    {
        StartCoroutine(StopSimulateAfterTime());
        transform.position = ball.transform.position;
        transform.forward = ball.transform.forward;
        computerCue.transform.position = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z - 28);
        computerCue.SetActive(true);
        transform.rotation = Quaternion.Euler(0, shootAngle, 0);
        isShooting = true;
        shootAngle = 0;
        holdAngle = -1;
        StartCoroutine(ExecuteAfterTime());
    }

    void StopControl()
    {
        computerCue.SetActive(false);
    }

    IEnumerator ExecuteAfterTime()
    {
        yield return new WaitForSeconds(time);
        isReady = true;
        yield return new WaitForSeconds(time + 0.01f);
        isReady = false;
        isShooting = false;       
        ball.Shoot(transform.forward, force * forceFactor, targetColor);
        forceFactor = 0.25f;
        holdForceFactor = 0;
        StopControl();
        yield return new WaitForSeconds(1f);
        ChangeBallStateByComputer(true);
    }

    private void CueShoot()
    {
        if (isReady)
        {
            computerCue.transform.position += computerCue.transform.up * speed * Time.deltaTime;
        }
        else
        {
            computerCue.transform.position -= computerCue.transform.up * speed * Time.deltaTime;
        }
    }

    private void Update()
    {
        if (isShooting)
        {
            CueShoot();
        }
    }

    public BallColor GetTargetColor()
    {
        return targetColor;
    }

    public void SetTargetColor(BallColor targetColor, List<BallController> TargetBalls)
    {
        this.targetColor = targetColor;
        this.TargetBalls = TargetBalls;
    }

    public void SetTargetColor(BallColor targetColor)
    {
        this.targetColor = targetColor;
    }

    public bool CheckList()
    {
        foreach (var item in TargetBalls)
        {
            if (item.ballState != BallState.NotActive)
            {
                return false;
            }
        }

        return true;
    }

    public void StartSimulate()
    {
        Physics.autoSimulation = false;
        transform.position = ball.transform.position;
        transform.forward = ball.transform.forward;
        transform.rotation = Quaternion.Euler(0, shootAngle, 0);
        ball.Shoot(transform.forward, force * forceFactor, targetColor);
        StartCoroutine(SimulateAfterTime());
    }

    IEnumerator SimulateAfterTime()
    {
        yield return new WaitForEndOfFrame();
        SimulateStartAction();
    }

    IEnumerator StopSimulateAfterTime()
    {
        yield return new WaitForEndOfFrame();
        SimulateEndAction();
    }

    public void ChangeAngle()
    {
        if (forceFactor < 1)
        {
            forceFactor += 0.25f;
            StartSimulate();
        }
        else
        {
            if (shootAngle < 360)
            {
                shootAngle += 10;
                forceFactor = 0.25f;
                StartSimulate();
            }
            else
            {
                if (holdAngle >= 0)
                {
                    shootAngle = holdAngle;
                    forceFactor = holdForceFactor;                   
                }              
                SimulateEnd();
            }
        }
    }

    public void HoldAngle()
    {
        holdAngle = shootAngle;
        holdForceFactor = forceFactor;
        ChangeAngle();
    }

    public void SimulateEnd()
    {
        Physics.autoSimulation = true;
        StopAllCoroutines();
        StartControl();
    }
}
