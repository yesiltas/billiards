﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject ground;

    public Control playercontrol;
    public ComputerControl computerControl;
    public BallManager ballManager;
    public WhiteBallManager whiteBall;

    public Action StartSliider;
    public Action<Turn> GameFinish;

    public Turn turn = Turn.Computer;

    private void Start()
    {
        playercontrol.ChangeBallState += ChangeBallState;

        computerControl.ChangeBallStateByComputer += ChangeBallState;
        computerControl.SimulateStartAction += SimulateStart;
        computerControl.SimulateEndAction += SimulateEnd;

        ballManager.ChangeTurn += ChangeTurn;
        ballManager.Foul += Foul;
        ballManager.CheckBalls += CheckBalls;
        ballManager.BlackInBasket += BlackInBasket;
        ballManager.ChangeAngle += ChangeAngle;
        ballManager.isNotFoul += isNotFoul;
        ballManager.CheckBallsSimulate += CheckBallsSimulate;

        whiteBall.NoCollisionFoul += NoCollisionFoul;
        whiteBall.StartControl += StartControl;
        whiteBall.StopContol += StopContol;
    }

    public void StartGame()
    {
        StartControl();
        ballManager.StartGame();
        whiteBall.StartGame();
    }

    public void Again()
    {
        turn = Turn.Player;
        playercontrol.Again();
        computerControl.Again();
        ballManager.Again();
    }

    public void SimulateStart()
    {
        ballManager.SimulateStart();
    }
    
    public void SimulateEnd()
    {
        ballManager.SimulateEnd();
    }

    public void ChangeBallState(bool ballsMoving)
    {
        ballManager.ChangeBallsState(ballsMoving);
    }

    private void StartControl()
    {
        if (turn == Turn.Player)
        {
            playercontrol.StartControl();
            StartSliider();
        }
        else
        {
            computerControl.StartSimulate();
        }
        ChangeBallState(false);
    }

    private void StopContol()
    {
        playercontrol.StopControl();
    }

    public void PrepareShot(float sliderValue)
    {
        playercontrol.PrepareShoot(sliderValue);
    }

    public void ChangeTurn()
    {
        Swap();
        StartControl();
    }

    private void Swap()
    {
        if (turn == Turn.Player)
        {
            turn = Turn.Computer;
        }
        else
        {
            turn = Turn.Player;
        }
    }

    private void Foul()
    {
        Swap();
        whiteBall.Replacement(turn);
        whiteBall.gameObject.SetActive(true);
        whiteBall.GetComponent<MeshRenderer>().enabled = true;
        if (turn==Turn.Computer)
        {
            StartControl();
        }
    }

    private void NoCollisionFoul()
    {
        Swap();
        whiteBall.CheckTurn(turn);
    }

    private void CheckBalls(List<BallColor> currentBallsInBasket)
    {
        BallColor targetColor = new BallColor();

        if (turn == Turn.Player)
        {
            targetColor = playercontrol.GetTargetColor();
        }
        else
        {
            targetColor = computerControl.GetTargetColor();
        }

        if (targetColor == BallColor.White)
        {
            BallColor tempColor = currentBallsInBasket[0];

            if (AllSameColor(currentBallsInBasket, tempColor))
            {
                if (turn == Turn.Player)
                {
                    List<BallController> targets = ballManager.GetTargetsList(tempColor);
                    playercontrol.SetTargetColor(tempColor, targets);
                    if (tempColor == BallColor.Red)
                    {
                        List<BallController> othertargets = ballManager.GetTargetsList(BallColor.Yellow);
                        computerControl.SetTargetColor(BallColor.Yellow, othertargets);
                    }
                    else
                    {
                        List<BallController> othertargets = ballManager.GetTargetsList(BallColor.Red);
                        computerControl.SetTargetColor(BallColor.Red, othertargets);
                    }
                }
                else
                {
                    List<BallController> targets = ballManager.GetTargetsList(tempColor);
                    computerControl.SetTargetColor(tempColor, targets);
                    if (tempColor == BallColor.Red)
                    {
                        List<BallController> othertargets = ballManager.GetTargetsList(BallColor.Yellow);
                        playercontrol.SetTargetColor(BallColor.Yellow, othertargets);
                    }
                    else
                    {
                        List<BallController> othertargets = ballManager.GetTargetsList(BallColor.Red);
                        playercontrol.SetTargetColor(BallColor.Red, othertargets);
                    }
                }
            }
            StartControl();
        }
        else
        {
            if (playercontrol.CheckList())
            {
                playercontrol.SetTargetColor(BallColor.Black);
            }

            if (computerControl.CheckList())
            {
                computerControl.SetTargetColor(BallColor.Black);
            }

            if (AllSameColor(currentBallsInBasket, targetColor))
            {
                StartControl();
            }
            else
            {
                ChangeTurn();
            }
        }

    }

    private static bool AllSameColor(List<BallColor> currentBallsInBasket, BallColor tempColor)
    {
        bool allSameColor = false;

        foreach (var item in currentBallsInBasket)
        {
            if (tempColor != item)
            {
                allSameColor = false;
                break;
            }
            allSameColor = true;
        }

        return allSameColor;
    }

    private void BlackInBasket()
    {
        switch (turn)
        {
            case Turn.Player:
                if (playercontrol.CheckList() == false)
                {
                    Swap();
                }
                break;

            case Turn.Computer:
                if (computerControl.CheckList() == false)
                {
                    Swap();
                }
                break;
        }
        FinishGame();
    }

    private void ChangeAngle()
    {
        computerControl.ChangeAngle();
    }

    private void isNotFoul()
    {
        computerControl.HoldAngle();
    }

    private void CheckBallsSimulate(List<BallColor> currentBallsInBasket)
    {
        BallColor targetColor = new BallColor();
        targetColor = computerControl.GetTargetColor();

        if (targetColor == BallColor.White)
        {
            BallColor tempColor = currentBallsInBasket[0];

            if (AllSameColor(currentBallsInBasket, tempColor))
            {
                computerControl.SimulateEnd();
            }
            else
            {
               ChangeAngle();
            }
        }
        else
        {
            if (AllSameColor(currentBallsInBasket, targetColor))
            {
                computerControl.SimulateEnd();
            }
            else
            {
                ChangeAngle();
            }
        }

    }

    public void StopRayCast()
    {
        ground.tag = "Untagged";
    }

    public void StartRayCast()
    {
        ground.tag = "Ground";
    }

    public void FinishGame()
    {
        GameFinish(turn);
    }
}

